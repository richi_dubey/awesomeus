//signup
const signupform = document.querySelector('#signup-form');

signupform.addEventListener('submit', (e) => {
    document.getElementById('SignUp_Modal_UniqueUsernameErrorDiv').style.display="none";
    document.getElementById('SignUp_button_loader').style.display='inline-block';
    e.preventDefault(); // to prevent it from going back, i.e. does not refreshes the page

    document.getElementById('errorSignUpDiv').style.display = 'none';

    //get user info
      const email = signupform['signup-email'].value;
      const password = signupform['signup-password'].value;
      const repassword = signupform['signup-repassword'].value;
      const uName = signupform['signup-name'].value;
      var regex = new RegExp("^[a-zA-Z0-9]*$");
      var regexSpace = new RegExp("^[\S]+$");

      if(!regex.test(uName)) {
        alert("Username should only contain numbers(0-9) and letters(A-Z or a-z). No spaces. Please try again :)");
        document.getElementById('SignUp_button_loader').style.display='none';
      }

      else {
        //signup the user
        firebase.database().ref('AllRegisteredUsernames_to_UserId/'+uName).once('value', function(snapshot){
          if(!snapshot.exists() && regex.test(uName)) {
            if (password == repassword) {
            auth.createUserWithEmailAndPassword(email, password).then(cred => {
              document.getElementById('SignUp_button_loader').style.display='none';
              document.getElementById('signup_tick_successful').style.margin="0 auto";
              document.getElementById('signup_tick_successful').style.display="block";
              document.getElementById('signup_text_inside_button_index').style.display="none";
              document.getElementById("signup-name").style.display="none";
              document.getElementById("modal_title_sign_up").innerHTML="Signed Up!";
              document.getElementById("SignUp_Modal_ModalBody_div_Check_email_signed_up").style.display="block";
              document.getElementById("signup-email").style.display="none";
              document.getElementById("small_text_sign_up_modal").style.display="none";
              document.getElementById("log_in_instead_sign_up_modal").style.display="none";
              document.getElementById("signup-password").style.display="none";
              document.getElementById("signup-repassword").style.display="none";
              document.getElementById("Or_in_sign_up_modal").style.display="none";
              document.getElementById("sign_up_with_google_div").style.display="none";

              console.log(cred.user);
              signupform.reset();
              var ErrorSignUpPara = document.getElementById("errorSignUpPara");
              var TextNodeErrorSignUp = document.createTextNode("");
              ErrorSignUpPara.appendChild(TextNodeErrorSignUp);
              var user = firebase.auth().currentUser;

              global_index_signed_up = 1;
              global_index_signed_up_username = uName;
            }).catch(err => {
                document.getElementById('errorSignUpDiv').style.display = 'block';
                var ErrorSignUpPara = document.getElementById("errorSignUpPara");
                ErrorSignUpPara.innerHTML = err.message;
                console.log("Error");
                console.log(err);
                document.getElementById('SignUp_button_loader').style.display='none';
              });
            } else {
              alert("The passwords do not match");
              document.getElementById('SignUp_button_loader').style.display='none';
            }
          } else {
            console.log("Username exists")
            var UniqueUsernameError=document.getElementById('SignUp_Modal_UniqueUsernameErrorDiv').style.display="block";
            document.getElementById('SignUp_button_loader').style.display='none';
          }
        });
      }
});