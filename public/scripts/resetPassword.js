var url = window.location.href;
console.log(url)

var oobCodeIndex = url.indexOf("oobCode=");

var apiKeyIndex = url.indexOf("&apiKey");

var actionCode= url.substring(oobCodeIndex+8, apiKeyIndex);

function handleResetPassword(auth, actionCode) {

    auth.verifyPasswordResetCode(actionCode).then((email) => {
      var accountEmail = email;

      firebase.database().ref('users').once('value', function(snapshot) {

        snapshot.forEach(function(childSnapshot) {

          if(childSnapshot.val().email == accountEmail) {
            console.log("Username written above is matched");

            document.getElementById("span-for-actual-username").innerHTML = childSnapshot.val().username;
            document.getElementById('checking_link_loader').style.display = "none";
            document.getElementById("big-div-for-username").style.display = "block";
            document.getElementById("PasswordForgotDiv").style.display = "block";
          }

        });
      });
    }).catch((error) => {
      document.getElementById('checking_link_loader').style.display = "none";
      document.getElementById('checking_link_not_valid').style.display="block";
    });
  
    auth.verifyPasswordResetCode(actionCode).then((email) => {
      var accountEmail = email;
      console.log(accountEmail)
      var newPassword = document.getElementById("newPassword").value;

      auth.confirmPasswordReset(actionCode, newPassword).then((resp) => {
        // Password reset has been confirmed and new password updated.
        console.log("Password has been reset")
        document.getElementById("PasswordForgotDiv").style.display="none";
        document.getElementById("PasswordResetSuccessful").style.display="block";

      }).catch((error) => {
        // Error occurred during confirmation. The code might have expired or the
        // password is too weak.
        console.log(error.message)
      });
    }).catch((error) => {
      // Invalid or expired action code. Ask user to try to reset the password
      // again.
      console.log(error.message)
      document.getElementById('PasswordReset').reset();
      alert(error.message)
    });
  }

  $(document).ready(function () {
    $("#PasswordReset").submit(function(e){
      checkPasswordOrientation();
      e.preventDefault();
    });
});


$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});


function checkPasswordOrientation() {
  var newPassword = document.getElementById('newPassword').value;
  var RenternewPassword = document.getElementById('RenternewPassword').value;
  if(newPassword==RenternewPassword)
  handleResetPassword(auth, actionCode);
  else
  alert("Two passwords don't match")
}

