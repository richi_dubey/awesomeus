$(".emojiButton").click(function() {
    var currentComm = $('#comment_InputImageModal').val();
    var newVal = currentComm + this.innerHTML;
    $('#comment_InputImageModal').val(newVal);
});

document.querySelector('.tooltipEmoji').onclick = function()
{
    console.log("Emoji Button Clicked")
    if(document.querySelector('.tooltiptext').style.visibility == "visible")
    {
        console.log("Visible Emojis")
        document.querySelector('.tooltiptext').style.visibility = "hidden";
        document.querySelector('.tooltiptext').style.opacity = "0";
    }
    else
    {
        document.querySelector('.tooltiptext').style.visibility = "visible";
        document.querySelector('.tooltiptext').style.opacity = "1";
    }
}