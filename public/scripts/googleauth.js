
var provider = new firebase.auth.GoogleAuthProvider();

function PopupSignInGoogle() {
  //console.log("Want a popup!");
  document.getElementById("google_signin_button_loader").style.display="block";
  firebase.auth()
  .signInWithPopup(provider)
  .then((result) => {
    /** @type {firebase.auth.OAuthCredential} */
    var credential = result.credential;

    document.getElementById('LogIn_Modal_ModalBody_div').style.display="none";
    document.getElementById('modal-title-log-in').innerHTML="Checking Details";
    document.getElementById('sign_in_modal_header_spinner').style.display="block";

    // This gives you a Google Access Token. You can use it to access the Google API.
    var token = credential.accessToken;
    // The signed-in user info.
    var user = result.user;
    console.log("User signed in successfully with Popup!");
    console.log("User is : ");
    console.log(user);
    console.log("token is : "+token);

    const user_auth = firebase.auth().currentUser;
	  global_google_sign_inorup = 1; //1 when user signs in or signs up with Google. This is important to


    if (user != null) {
      console.log("Yes user signed in with email:");
      console.log(user_auth.email);
      
        // User is signed in
    } else {
      console.log("User has not signed in!!");
        // No user is signed in
    }
    // ...
  }).catch((error) => {
    
    document.getElementById("google_signin_button_loader").style.display="none";
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;

    console.log(error.code);
    console.log(error.message);

    if(error.code.localeCompare("auth/popup-closed-by-user")!=0)
      alert("Something went wrong. Please try again. We promise it'll work this time.");

    // ...
  });
}


function PopupSignUpGoogle() {
    //console.log("Want a popup!");
    document.getElementById("google_signup_button_loader").style.display="block";
    firebase.auth()
    .signInWithPopup(provider)
    .then((result) => {
      /** @type {firebase.auth.OAuthCredential} */
      var credential = result.credential;
      
      document.getElementById('SignUp_Modal_ModalBody_div').style.display="none";
      document.getElementById('modal_title_sign_up').innerHTML="Checking Details  ";
      document.getElementById('sign_up_modal_header_spinner').style.display="block";
  
      // This gives you a Google Access Token. You can use it to access the Google API.
      var token = credential.accessToken;
      // The signed-in user info.
      var user = result.user;
      console.log("User signed in successfully with Popup!");
      console.log("User is : ");
      console.log(user);
      console.log("token is : "+token);
  
      const user_auth = firebase.auth().currentUser;
        global_google_sign_inorup = 1; //1 when user signs in or signs up with Google. This is important to
  
  
      if (user != null) {
        console.log("Yes user signed in with email:");
        console.log(user_auth.email);
        
          // User is signed in
      } else {
        console.log("User has not signed in!!");
          // No user is signed in
      }
      // ...
    }).catch((error) => {
      
      document.getElementById("google_signup_button_loader").style.display="none";
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;

      console.log(error.code);
      console.log(error.message);
      if(error.code.localeCompare("auth/popup-closed-by-user")!=0)
        alert("Something went wrong. Please try again. We promise it'll work this time.");
      // ...
    });
}

document.getElementById('google_select_username_modal_form').addEventListener('submit', (e) => {
    e.preventDefault(); //TODO: Check if this is needed.
    
    document.getElementById('google_select_username_modal_UniqueUsernameErrorDiv').style.display="none";
    document.getElementById('google_select_username_modal_spinner_inside_signup_button_span').style.display="inline-block";

    console.log("User is trying to to sign up");

    const uName = document.getElementById('google_select_username_modal_username_input').value;

    var regex = new RegExp("^[a-zA-Z0-9]*$");
    
    if(!regex.test(uName)) {
      alert("Username should only contain numbers(0-9) and letters(A-Z or a-z). No spaces. Please try again :)");
    }
    
    firebase.database().ref('AllRegisteredUsernames_to_UserId/'+uName).once('value', function(snapshot){
    
        if(!snapshot.exists() && regex.test(uName)) {
            //New username chosen is good. Proceed to register the username (and other details) in database
            console.log("No issue in new username. Proceed to register user in database");

            const user = firebase.auth().currentUser;

            document.getElementById('google_select_username_modal_signup_tick_successful_img').style.margin="0 auto";
            document.getElementById('google_select_username_modal_signup_tick_successful_img').style.display="block";
            document.getElementById('google_select_username_modal_title_h5').innerHTML="Signed Up!";
            document.getElementById('google_select_username_modal_username_input').style.display="none";
            document.getElementById('google_select_username_modal_spinner_inside_signup_button_span').style.display='none';
            document.getElementById('google_select_username_modal_sign_up_text_inside_button_span').innerHTML="";

            register_new_user_database(user,uName);
        } else {
            console.log("Username exists")
            
            document.getElementById('google_select_username_modal_UniqueUsernameErrorDiv').style.display="block";
            document.getElementById('google_select_username_modal_spinner_inside_signup_button_span').style.display='none';
        }
    });
});