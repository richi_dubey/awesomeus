document.getElementById('myImg_button').setAttributeNode(document.createAttribute("disabled"));
document.getElementById('allImg_button').setAttributeNode(document.createAttribute("disabled"));


auth.onAuthStateChanged(user =>{
    if(!user) {
        global_LoggedIn = 0;
        console.log('User Not Logged In! ');

        console.log("Opened: "+window.location.href);
        var path = window.location.href;
        var split = path.split("/");
        var x = split.slice(0, split.length - 1).join("/") + "/";

        console.log("To be replaced with : "+ x+"index.html");


        window.location.replace(x+"index.html");
    } else {

        //First check for the case: Authenticated but user data not available in database - happens when someone signs up with
        //Google auth but closes connection right before selecting a username (hence does not get registered)
        
        console.log(user.uid);
        
        firebase.database().ref('users/'+user.uid).once('value').then((snapshot)=>{
            if(snapshot.exists()) {
                global_LoggedIn = 1;

                if(document.getElementById("email-id-user-toast")) { //Condition since usersetting is also using this script, but it doesnt show any toast. 
                    document.getElementById("email-id-user-toast").innerHTML = user.email;
                    if (!user.emailVerified) {
                    // email is not verified. Show toast.
                        $('#email_not_verified_toast').toast('show');
                    } else {
                        $('#all_toast_elements').remove();
                    }
                }
            
            } else {
                alert("Something went wrong with your account registration. Our minions messed up. Please sign up again.")
                firebase.auth().signOut();
            }
        });
    }
});