function repasswordkey() {
	var elem=document.getElementById("signup-repassword");
	if(document.getElementById("signup-password").value==elem.value) {
		elem.style.outlineColor="green";
	}
	else {
		elem.style.outlineColor="red";}
  }

function checkEmailFormatting()
{
   var SignUpEmail = document.getElementById("signup-email");
   var SignUpEmailValue = document.getElementById("signup-email").value;
   var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+")  )@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      if(regex.test(SignUpEmailValue))
         SignUpEmail.style.color="green";
      else
         SignUpEmail.style.color="red";
}

   function CheckSpace(event)
   { var elem=document.getElementById("spaceSignUpname");

      if(event.which ==32 || (event.which >=33 && event.which <=47) || (event.which >=58 && event.which <=64) )
      {  elem.style.display="block";
         event.preventDefault();
         return false;
      }
      else
      elem.style.display="none";
   }
