function upvote(names, users, views_para, upvotes_para, name_para, textdiv, upvote_btn, img_author, childKey) {
    firebase.database().ref('Upvotes/' + childKey + '/' + users.uid).once('value', function(snapshot) {
        if(!snapshot.exists())
        {
            upvote_btn.style.backgroundPosition='0px 2px';
            var updates = {};
            updates['Upvotes/' + childKey + '/'+users.uid] = users.uid;
            name_para.innerHTML="";
            name_para.innerHTML=img_author;
            textdiv.appendChild(name_para);
            console.log(childKey + "Upvoted")
            return firebase.database().ref().update(updates);
        }
        else
        {
            upvote_btn.style.backgroundPosition='0px -25px';
            firebase.database().ref('Upvotes/'+childKey+'/'+users.uid).remove();
            console.log(childKey + "Unvoted")
            name_para.innerHTML="";
            name_para.innerHTML=img_author;
            textdiv.appendChild(name_para);
        }
    });
}