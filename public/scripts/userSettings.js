var coll = document.getElementsByClassName("collapsible");
var i;
var num_for_reauth_action=0;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  });
}

const auth_userSettings = firebase.auth();
const logout = document.querySelector('#logout_btn_user_settings');
logout.addEventListener('click',(e) => {
   e.preventDefault();
   auth_userSettings.signOut().then(() =>{
   console.log('User Signed Out');
   window.location.replace("index.html");
 });
});

firebase.database().ref('users').once('value', function(snapshot) {
  var currentUser = firebase.auth().currentUser;

  console.log("Current user is: ");
  console.log(currentUser);

  snapshot.forEach(function(childSnapshot) {

    if( childSnapshot.val().email == currentUser.email) {
      var username_user_settings = childSnapshot.val().username;
      document.getElementById("username_user_settings").innerHTML = username_user_settings;
      document.getElementById("email_user_settings").innerHTML = childSnapshot.val().email;
      $(".loader-profile-section"). css("display", "none");
    }

  });

});

const ChangePasswordForm = document.querySelector('#ChangePasswordForm');
ChangePasswordForm.addEventListener('submit', (e) => {
	e.preventDefault(); // to prevent it from going back, i.e. does not refreshes the page
	document.getElementById('errorChangePassDiv').style.display = 'none';
	//get password info
	const password = ChangePasswordForm['change_password'].value;
	const repassword = ChangePasswordForm['change_repassword'].value;
	//change user password
	if (password == repassword) {
		var user = firebase.auth().currentUser;
		user.updatePassword(password).then(function () {
			console.log('Password Changed Successfully')
			$("#ChangePasswordModal").modal('hide');
			alert("Password Changed Successfully")
			ChangePasswordForm.reset();
		}).catch(err => {
			// An error happened.
			document.getElementById('errorChangePassDiv').style.display = 'block';
			var ErrorChangePasswordPara = document.getElementById("errorChangePassPara");
			ErrorChangePasswordPara.innerHTML = err.message;
			console.log("Error")
		});
	} else {
		alert("The passwords do not match");
	}
});

function Reauthenticate(password, number, ReauthenticationForm) {
  const user = firebase.auth().currentUser;
  // TODO(you): prompt the user to re-provide their sign-in credentials
  const credential = firebase.auth.EmailAuthProvider.credential(
    user.email,
    password
  );

  user.reauthenticateWithCredential(credential).then(() => {
    // User re-authenticated.
    document.getElementById("text_inside_button_reauth").style.display="block";
    document.getElementById("spinner_inside_button_reauth").style.display="none";

    console.log("Done")
    $("#ReauthenticationModal").modal("hide")
    if(number==1)
      $("#modalDelete_All_Images").modal();
    else if(number==2)
      $("#modalDelete_Account").modal();
    else if(number==3)
      $("#modalDelete_All_Images_Account").modal();
    ReauthenticationForm.reset();

  }).catch((error) => {
    // An error ocurred
    // ...
    document.getElementById("text_inside_button_reauth").style.display="block";
    document.getElementById("spinner_inside_button_reauth").style.display="none";
    console.log(error.message)
    alert(error.message);
  });
}

function ReauthenticateActions(number) {
  document.getElementById('reauthentication-mail').innerHTML = firebase.auth().currentUser.email;
  $("#ReauthenticationModal").modal("show");
  num_for_reauth_action = number;
}

const ReauthenticationForm = document.querySelector("#reauthentication-form-modal");
ReauthenticationForm.addEventListener('submit', (e) => {
    e.preventDefault();
    console.log("Reauth Button Pressed");

    document.getElementById("text_inside_button_reauth").style.display="none";
    document.getElementById("spinner_inside_button_reauth").style.display="block";

    const password = ReauthenticationForm['reauthentication-password'].value;
    Reauthenticate(password, num_for_reauth_action, ReauthenticationForm);
  })

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})