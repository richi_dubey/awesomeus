
// Login Form
const loginform = document.querySelector('#login-form-modal');
const loginButton = document.querySelector('#login-button');

loginform.addEventListener('submit', (e) => {
  if(global_google_sign_inorup == 1) {
    document.getElementById('Login_button_loader').style.display='inline-block';
    e.preventDefault();
    //User is signing up with a new username. Proceed to check if username is available.

    console.log("User tried to sign up");

    const uName = loginform['google_signup_username'].value;

    var regex = new RegExp("^[a-zA-Z0-9]*$");

    if(!regex.test(uName))
      alert("Username should only contain numbers(0-9) and letters(A-Z or a-z). No spaces. Please try again :)");

      firebase.database().ref('AllRegisteredUsernames_to_UserId/'+uName).once('value', function(snapshot){
        if(!snapshot.exists() && regex.test(uName)) {
          //New username chosen is good. Proceed to register the username (and other details) in database
          console.log("No issue in new username. Proceed to register user in database");

          const user = firebase.auth().currentUser;

          document.getElementById('login_tick_successful').style.margin="0 auto";
          document.getElementById('login_tick_successful').style.display="block";
          document.getElementById('modal-title-log-in').innerHTML="Signed Up!";
          document.getElementById("LogIn_Modal_ModalBody_div_Check_email_signed_up").style.display = "block";

          register_new_user_database(user,uName);
        } else {
          console.log("Username exists")

          var UniqueUsernameError=document.getElementById('UniqueUsernameErrorDiv').style.display="block";
          document.getElementById('Login_button_loader').style.display='none';
        }
    });
  } else {
    //User is trying to login.
    document.getElementById('Login_button_loader').style.display='inline-block';
    e.preventDefault();
    //get User info
    const email = loginform['login-email'].value;
    const password = loginform['login-password'].value;

    document.getElementById("errorLogInPara").innerHTML = "";
    emailOrUsername(email, password, loginform);
  }
});

function SignInFirebase(loginID, password, loginform)
{
  auth.signInWithEmailAndPassword(loginID, password).then(cred => {
    //console.log(cred.user)  //not needed really
     document.getElementById('Login_button_loader').style.display="none";
     document.getElementById('login_tick_successful').style.margin="0 auto";
     document.getElementById('login_tick_successful').style.display="block";
     document.getElementById('modal-title-log-in').innerHTML="Logged In!";
     document.getElementById('small-text-login-modal').style.display="none";
     document.getElementById('login-email').style.display="none";
     document.getElementById('login-password').style.display="none";
     document.getElementById('forgot-password-hyperlink-login-modal').style.display="none";
     document.getElementById('sign-up-instead-login-modal').style.display="none";
     document.getElementById('login_text_inside_button_index').style.display="none";
     document.getElementById('sign_in_with_google_div').style.display="none";
     document.getElementById('Or_in_sign_in_modal').style.display="none";
     loginform.reset();
     var ErrorLogInPara = document.getElementById("errorLogInPara");
     var TextNodeErrorLogIn = document.createTextNode("");
     ErrorLogInPara.appendChild(TextNodeErrorLogIn);
    }).catch(err => {
     document.getElementById('errorLogInDiv').style.display = 'block';
     var ErrorLogInPara = document.getElementById("errorLogInPara");
     if(err.message == "The password is invalid or the user does not have a password.")
        ErrorLogInPara.innerHTML="The password is invalid";
     else if(err.message == "There is no user record corresponding to this identifier. The user may have been deleted.")
        ErrorLogInPara.innerHTML="Enter Details correctly or create a new account";
     else if(err.message == "An internal error has occurred.")
        ErrorLogInPara.innerHTML="Please refresh the page!";
     else if(err.message == "The supplied auth credential is malformed or has expired.")
        ErrorLogInPara.innerHTML="Please refresh the page!";
     else if(err.message == "A network error (such as timeout, interrupted connection or unreachable host) has occurred.")
        ErrorLogInPara.innerHTML="Please refresh the page!";
     else
        ErrorLogInPara.innerHTML = err.message;
     console.log("Error")
     document.getElementById('Login_button_loader').style.display='none';
   });
}

function emailOrUsername(email, password, loginform) {
  if(email.indexOf('@')!=-1) {
    SignInFirebase(email, password, loginform);
  }
  else
  {
    firebase.database().ref('AllRegisteredUsernames_to_UserId/'+email).once('value', function(snapshot){
      const usernameMail = snapshot.val();
      if(usernameMail!=null) {
          firebase.database().ref('users/'+usernameMail).once('value', function(childSnapshot){

            if(childSnapshot.val() == null) {
              console.log("Error")

              document.getElementById("errorLogInPara").innerHTML = "Something is wrong with this website. Please contact the admin with a screenshot of this page at admin@awesomeus.tech";
              document.getElementById('Login_button_loader').style.display='none';
            }
            loginID = childSnapshot.val().email;
            SignInFirebase(loginID, password, loginform);

          }).catch(err => {
          console.log("Error")

          document.getElementById("errorLogInPara").innerHTML = err.message;
          document.getElementById('Login_button_loader').style.display='none';
          });
      } else {
        document.getElementById('Login_button_loader').style.display='none';
        document.getElementById('errorLogInDiv').style.display = 'block';
        var ErrorLogInPara = document.getElementById("errorLogInPara");
        ErrorLogInPara.innerHTML="Username does not exist";

      }
     })
  }
}


function PopupSignInGoogle() {
  console.log("Want a popup!")
  firebase.auth()
  .signInWithPopup(provider)
  .then((result) => {
    /** @type {firebase.auth.OAuthCredential} */
    var credential = result.credential;

    // This gives you a Google Access Token. You can use it to access the Google API.
    var token = credential.accessToken;
    // The signed-in user info.
    var user = result.user;
    console.log("User signed in successfully with Popup!");
    console.log("User is : ");
    console.log(user);
    console.log("token is : "+token);

    const user_auth = firebase.auth().currentUser;
	  global_google_sign_inorup = 1; //1 when user signs in or signs up with Google. This is important to


    if (user != null) {
      console.log("Yes user signed in with email:");
      console.log(user_auth.email);

        // User is signed in
    } else {
      console.log("User has not signed in!!");
        // No user is signed in
    }
    // ...
  }).catch((error) => {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
    // ...
  });
}
