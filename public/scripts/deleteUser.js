    function deleteUser()
    {
      var user = firebase.auth().currentUser;
      //firebase.database().ref('users/' + user.uid).remove();

           //alert(user.email)
        firebase.database().ref('AllRegisteredUsernames_to_UserId').once('value', function(snapshot){
          snapshot.forEach(function(childSnapshot){
            if(childSnapshot.val()==user.uid) {
              firebase.database().ref('AllRegisteredUsernames_to_UserId/' + childSnapshot.key).remove().then(function(){
                console.log("Deleted from All registered username");

                firebase.database().ref('users/' + user.uid).remove().then(function(){
                  console.log("Deleted from users database");

                  user.delete().then(() =>{
                    window.location.replace("index.html");
                    alert("User Deleted");
                  });
                  });
              });
            }
          })
        })
    }
