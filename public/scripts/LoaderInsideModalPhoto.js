var reqId=null; //IMP: Global variable, also used in modalPhoto.js
let start;
const elem=document.querySelector("span");
const finalPos=200;
const total=2000;
let progress;
let position;
let easing;

const circle=document.querySelector("circle");;
const path=document.querySelector("#loader_inside_modal_path");
console.log("Loader path is: "+path);
const pathLength=path.getTotalLength();

function getProg(elapsed) 
{
    return Math.min(elapsed/total,1);
}

function easeOut(progress) 
{
  //  return Math.pow(--progress,5)+1;
  return progress*(2-progress);
}

function draw(timestamp) 
{
    //console.log("Requested");
    if(start==undefined)
        start=timestamp;

    const elapsed=timestamp-start;
   // console.log("Hi " + elapsed);

    progress= getProg(elapsed);
    progress=easeOut(progress);
    const {x,y} = path.getPointAtLength(pathLength*progress);
    
    circle.setAttribute("cx",x);
    circle.setAttribute("cy",y);

    if(progress==1)
        start=timestamp;

    easing = easeOut(progress);
    position = easing*finalPos;
    //elem.style.transform='translate('+position+'px,'+position+'px)';
    //elem.style.transform='translateY('+position+'px)';
    reqId=window.requestAnimationFrame(draw);
}

// window.requestAnimationFrame(draw);