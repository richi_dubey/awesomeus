var storageRef = firebase.storage().ref();
function deletePhotos()
{
	var listRef_img = storageRef.child('images');
       listRef_img.listAll().then(function(res) {
		res.items.forEach(function(itemRef) { 
		var img_full_name=itemRef.name;	
		var image_count = img_full_name.indexOf(firebase.auth().currentUser.uid)
		if(image_count!=-1)
		{
			var DelRef = storageRef.child('images'+'/'+img_full_name);
			DelRef.delete().then(function() {
			// File deleted successfully
			console.log("Images Deleted")
		  }).catch(function(error) {
			// Uh-oh, an error occurred!
		  });
		}
	});
});
}

function deletePhotosDatabase()
{
	firebase.database().ref('/images').once('value', function(snapshot) {
		snapshot.forEach(function(childSnapshot) {
			var childKey = childSnapshot.key;
			var childData = childSnapshot.val();
			console.log("Entry :"+childSnapshot.key);
			console.log("Entry :"+childSnapshot.val().author);

			if(childSnapshot.val().author == firebase.auth().currentUser.uid)
			{
				firebase.database().ref('/images/'+childSnapshot.key).remove();
				console.log("Deleted"+childSnapshot.val().name);
			}
		});
	});
	setTimeout(function() {
		location.reload();				
		}, 1000);
}
