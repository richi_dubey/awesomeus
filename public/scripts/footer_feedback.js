$(".footer_button").click(function(){
    $('#Footer_feedback_modal').modal();
  });
  
const footer_feedback_form = document.querySelector('#footer_feedback_form');

footer_feedback_form.addEventListener('submit', (e) => {
    e.preventDefault();

    console.log("Form Submitted")
    var feedback_comment = document.getElementById('footer_feedback_form_comment').value;

    // Get a key for a new Post.
    var newFeedbackKey = firebase.database().ref().child('Feedback').push().key;
    console.log("Feedback key is " );
    console.log(newFeedbackKey);
    if(global_LoggedIn == 1)
    {
        console.log("User logged in");
        const uid = firebase.auth().currentUser.uid;
        firebase.database().ref('users/'+uid).once('value').then((snapshot)=>{
            if(snapshot.exists()) {//should always be true
                newFeedbackKey = newFeedbackKey + '(' + snapshot.val().username+')';

                $('#Footer_feedback_modal').modal('hide');
                
                document.getElementById('footer_feedback_form').reset();
                
                return firebase.database().ref("Feedback/"+newFeedbackKey).set({
                    Feedback: feedback_comment
                });

            } else{
                alert("Feedback not submitted. Something is wrong. Please contact admin@awesomeus.tech");
            }
        }).catch((error)=>{
            alert("Feedback not submitted. Something is wrong. Please contact admin@awesomeus.tech and tell error: "+error);
        });
    } else {
        $('#Footer_feedback_modal').modal('hide');
        document.getElementById('footer_feedback_form').reset();
        return firebase.database().ref("Feedback/"+newFeedbackKey).set({
            Feedback: feedback_comment
        });
    }
});