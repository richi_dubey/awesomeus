//for checking the status of the user i.e whether he is logged in or not
auth.onAuthStateChanged(user => {
    if (user) {
        global_LoggedIn=1;
     }
     else {
         global_LoggedIn=0;
     }
});