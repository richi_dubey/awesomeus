$('.btn-upload').click(function(){ $('#imgupload').trigger('click'); });


$("#imgupload").change(function() {
    var userId = firebase.auth().currentUser.uid;

    var d=new Date();

    console.log("Hours: "+d.getHours() + " Minutes: "+d.getMinutes() + " Seconds: "+d.getSeconds()+ "Day: "+d.getDate() + " Month: "+d.getMonth() + " Year: "+d.getFullYear());

    var imgdate={
    hour  : ("0" + (d.getHours())).slice(-2),
    min   : ("0" + (d.getMinutes())).slice(-2),
    sec   : ("0" + (d.getSeconds())).slice(-2),
    day   : ("0" + d.getDate()).slice(-2),
    month : ("0" + (d.getMonth() + 1)).slice(-2),
    year  : d.getFullYear()
    };

    console.log(imgdate);

    var defimgdiv=document.querySelector("#def-upload-img");
    var defimg=document.createElement("img");
    defimg.setAttribute("class", "img-upload");
    defimg.setAttribute("id", userId+"@"+this.files[0].name);

    var reader = new FileReader();
    reader.onload = function(){
        var dataURL = reader.result;
        defimg.src = dataURL;

        if(defimgdiv.childElementCount) {
            defimgdiv.removeChild(defimgdiv.childNodes[0]);
        }

        defimgdiv.appendChild(defimg);
    };
    reader.readAsDataURL(this.files[0]);

    var utext = document.querySelector(".btn-upload");

    //utext.innerText="Loaded "+this.files[0].name + " -- ";
    utext.innerText="Load a different Image";
    console.log("Read " + utext.innerText );

    document.querySelector(".add-name-placeholder").style.display = "block";
    document.querySelector(".form-name").style.display = "block";

    var fname = userId+"@"+this.files[0].name;
    var file = this.files[0]; // use the Blob or File API
    var floc="images/"+fname;


    $('.btn-upload-final').click(function() {

        document.querySelector(".loader").style.display="inline-block";
        document.querySelector(".uploading-text").style.display="inline-block";
        document.querySelector('.btn-upload-final').toggleAttribute("disabled");
        utext.innerText="Uploading ...";
        document.querySelector(".btn-upload").style.cursor="default";

        var tags=document.querySelectorAll(".tag-input");
            for( var i=0;i<tags.length;i++)
                { tags[i].toggleAttribute("disabled"); }

        document.querySelector('.name-input').toggleAttribute("disabled");
        document.querySelector('.btn-upload').toggleAttribute("disabled");
        document.querySelector('.btn-close-upload-modal-bottom').toggleAttribute("disabled");

        document.querySelector(".img-upload").style.opacity=.2;

        if(defimgdiv.childNodes[0].id != fname)
            return;

        console.log("Uploading a file " + fname);
                        //  firebase.analytics();
        var storage = firebase.storage();
        var storageRef = storage.ref();			

        var tags=[];

        var trav=document.querySelectorAll(".tag-input");
        var index;

        for ( index=0; index<trav.length;index++) {
            console.log(trav[index].value);
                
            if(trav[index].value != "") {
                tags.push(trav[index].value);
            }
        }

        // An image entry.
        var postImg = {
            name: fname,
            realname: document.querySelector(".name-input").value,
            tags: tags,
            date: imgdate,
        };

        var newPostKey = firebase.database().ref().child('images').push().key;

        // Write the new post's data simultaneously in the posts list and the user's post list.
        var updates = {};
        updates['Image/' + userId +'/' + newPostKey] = postImg;
        //updates['/user-posts/' + uid + '/' + newPostKey] = postData;

        firebase.database().ref().update(updates).then(function(){

            storageRef.child(floc).put(file).then(function(snapshot) {
            
            console.log('Uploaded a blob or file!');
            utext.innerText="Uploaded "+fname +"!!";

            setTimeout(function() {
                location.reload();				
                }, 1000);
            });
        });
    });
});
