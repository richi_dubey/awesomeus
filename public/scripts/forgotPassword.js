const forgotPasswordForm = document.querySelector("#forgotPasswordForm");
forgotPasswordForm.addEventListener('submit', (e) => {
 console.log("Email Button Pressed")
 const emailAddress = forgotPasswordForm['forgot-email'].value;
 e.preventDefault();
 emailOrUsernameForgotPassword(emailAddress);
})

function forgotPasswordFirebase(emailUsername) {
  var auth = firebase.auth();
  auth.sendPasswordResetEmail(emailUsername).then(() => {
    // Email sent.
    var mask="";
    var maskedEmail
    if(emailUsername.length>4)
    {
      for(var i = 2; i<=emailUsername.indexOf('@')-3;i++) {
        mask=mask+'*';
      }
      maskedEmail = emailUsername.substring(0,2)+mask+emailUsername.substring(emailUsername.indexOf('@')-2, emailUsername.length);
    }

    else
    {
      maskedEmail=emailUsername;
    }
    $('#ForgotPasswordModal').modal('hide');
    alert("Email Sent on " + maskedEmail)
  }).catch(function (error) {
    // An error happened.
    alert("Email not sent. Possible Error: User does not exists or the email entered is invalid.")
    console.log(error.message)
  });
}

function emailOrUsernameForgotPassword(email) {
  if(email.indexOf('@')!=-1)
    forgotPasswordFirebase(email);
  else
  {
    firebase.database().ref('AllRegisteredUsernames_to_UserId/'+email).once('value', function(snapshot){
      const usernameMail = snapshot.val();
      if(usernameMail!=null) {
        firebase.database().ref('users/'+usernameMail).once('value', function(childSnapshot){
          emailForLoginID = childSnapshot.val().email;
          forgotPasswordFirebase(emailForLoginID);
        })
      } else {
        alert("Username does not exist")
      }
    })
  }
}