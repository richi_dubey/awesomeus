//------------------------------------------------------Start Sorting Logic-------------------------------------------------------------------
function sort_images(caller){
    var btn_sort = document.getElementById("btn-sort");

    btn_sort.innerHTML = caller.innerHTML;

    printPhotos(btn_sort.innerHTML.toString());
}

function printPhotos(order)
{
	src_img.innerHTML="";

	if(order.localeCompare("Most Upvoted")==0) {

		let upv_src_img_list;
		upv_src_img_list=[]
		upv_src_img_list=Array.from(src_img_list);

	//	console.log(upv_src_img_list);

		upv_src_img_list.sort(function (firstImg, secondImg) {

			//console.log(secondImg.childNodes[2]);

			if(bool_my_img.localeCompare("true")==0)
			{
				return secondImg.childNodes[2].childNodes[3].innerHTML - firstImg.childNodes[2].childNodes[3].innerHTML;
			}
			else
			{
				return secondImg.childNodes[1].childNodes[3].innerHTML - firstImg.childNodes[1].childNodes[3].innerHTML;
			}

		});

		for(var img_itr=0; img_itr<upv_src_img_list.length;img_itr++) {
			src_img.appendChild(upv_src_img_list[img_itr]);
		}
	}

	else if(order.localeCompare("Most Viewed")==0) {

		let vie_src_img_list;

		vie_src_img_list=[];
		vie_src_img_list=Array.from(src_img_list);
		vie_src_img_list.sort(function (firstImg, secondImg) {
		//	console.log(secondImg);

			//console.log("View is "+secondImg.childNodes[1].childNodes[4].innerHTML.match(/\d/g) + " from "+secondImg.childNodes[1].childNodes[4].innerHTML);
			if(bool_my_img.localeCompare("true")==0)
				return secondImg.childNodes[2].childNodes[4].innerHTML - firstImg.childNodes[2].childNodes[4].innerHTML;
			else
				return secondImg.childNodes[1].childNodes[4].innerHTML - firstImg.childNodes[1].childNodes[4].innerHTML;
		});

		for(var img_itr=0; img_itr<vie_src_img_list.length;img_itr++) {
			src_img.appendChild(vie_src_img_list[img_itr]);
		}
	}

	else if(order.localeCompare("Random")==0) {

		let ran_src_img_list;
		ran_src_img_list=[]
		ran_src_img_list=Array.from(src_img_list);

			ran_src_img_list.sort(function (firstImg, secondImg) {
				if( Math.floor(Math.random() * 2) < 1)
					return -1;
				else return 1;
		});
		for(var img_itr=0; img_itr<src_img_list.length;img_itr++) {
			src_img.appendChild(ran_src_img_list[img_itr]);
		}
	}

	else if(order.localeCompare("Recently Added")==0) {

		let date_src_img_list;

		date_src_img_list=[];
		date_src_img_list=Array.from(src_img_list);
		date_src_img_list.sort(function (firstImg, secondImg) {
		//	console.log(secondImg);

			//console.log("View is "+secondImg.childNodes[1].childNodes[4].innerHTML.match(/\d/g) + " from "+secondImg.childNodes[1].childNodes[4].innerHTML);
			if(bool_my_img.localeCompare("true")==0) {
				if(secondImg.childNodes[2].childNodes[5].innerHTML.localeCompare("NULL")==0 || firstImg.childNodes[2].childNodes[5].innerHTML.localeCompare("NULL")==0) {
					if(secondImg.childNodes[2].childNodes[5].innerHTML.localeCompare("NULL")==0 && firstImg.childNodes[2].childNodes[5].innerHTML.localeCompare("NULL")==0) {
						if( Math.floor(Math.random() * 2) < 1)
							return -1;
						else return 1;
					}
					else {
						if(secondImg.childNodes[2].childNodes[5].innerHTML.localeCompare("NULL")==0)
							return -1; // Sort puts secondElement first when >0 num is returned.
						else return 1;
					}
				}
				else return secondImg.childNodes[2].childNodes[5].innerHTML - firstImg.childNodes[2].childNodes[5].innerHTML;
			}
			else {
				if( (secondImg.childNodes[1].childNodes[5].innerHTML.localeCompare("NULL")==0) || (firstImg.childNodes[1].childNodes[5].innerHTML.localeCompare("NULL")==0)) {
					if((secondImg.childNodes[1].childNodes[5].innerHTML.localeCompare("NULL")==0) && (firstImg.childNodes[1].childNodes[5].innerHTML.localeCompare("NULL")==0)) {
						if( Math.floor(Math.random() * 2) < 1)
							return -1;
						else return 1;
					}
					else {
						if(secondImg.childNodes[1].childNodes[5].innerHTML.localeCompare("NULL")==0)
							return -1; // Sort puts secondElement first when >0 num is returned.
						else return 1;
					}
				}
				else return secondImg.childNodes[1].childNodes[5].innerHTML - firstImg.childNodes[1].childNodes[5].innerHTML;
			}
		});

		for(var img_itr=0; img_itr<date_src_img_list.length;img_itr++) {
			src_img.appendChild(date_src_img_list[img_itr]);
		}
	}

}

//------------------------------------------------------End Sorting Logic-------------------------------------------------------------------