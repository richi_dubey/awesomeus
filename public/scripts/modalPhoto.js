var reqId = null;

var already_closed = 0; //0 if false, 1 if true. Set to 0 in load_modal, to 1 in modal_gets_closed. Gets used while checking both var - since we do not want
//to disable the loader animation display if the image is already closed - to allow the next image that would be opened to have an svg animation.

/////////////////////////////////////////////////////////////// ** START: Closing modal ** /////////////////////////////////////////////////////////////////////

function modal_gets_closed(){

    already_closed = 1;

    console.log("Closing Reqid for animation inside modal is (abnormal): ");
    console.log(reqId);
    window.cancelAnimationFrame(reqId); //To stop animation, - needed in case someone closes modal as soon as opening it.
    $('.modale').removeClass('opened');
    document.getElementById("btn-close-image").style.display="none";
    $('.btn-close-image').css('display','none');
    document.querySelector("body").style.overflow = 'visible'; ///Ye background scroll wapis enable krne ke lie hai.
    document.querySelector("#loader_inside_modal_svg").style.display="block";
    document.querySelector(".modal-dialog-image").style.maxHeight="600px"; //To avoid modal height resizing and to show 'A' loader properly
}

$('.closemodale').click(function (e) {
    //e.preventDefault();
    modal_gets_closed();
});

$(document).keydown(function(event) {
    if (event.keyCode == 27) {
        //event.preventDefault(); //Iska kya logic hai yaha pe?
        modal_gets_closed();
    }
  });

  window.onclick = event => { //Duplicate from loggedin-dropdown. needed here in case user is not loggedin.
    if (event.target.getAttribute('class') == "modale opened") {
        modal_gets_closed();
    }
}

function btn_close_clicked_modal_photo(){
    modal_gets_closed();
}


var bool_big_image_loaded = "false";
var bool_all_comments_loaded = "false";
var num_curr_comment = 0;

/////////////////////////////////////////////////////////////// ** END: Closing modal ** /////////////////////////////////////////////////////////////////////

// Logic Explanation: This js file uses two global boolean variables: bool_big_image_loaded and bool_all_comments_loaded. Only when both the variables are true,
// the loader stops and displays the image. So, these 2 variables are checked twice - once when images gets loaded and once when all the comments get loaded.

//The variable num_curr_comment is used to check if we have read and printed all the comments, if so - call the function that stops loader.
///////////////////

var placebo_image;

function image_loaded() {
    bool_big_image_loaded = "true";
    console.log("bool big img loaded variable set");
    both_bool_vars_check(); //Check if both are true now. This function call is imp when comments are loaded before the image gets loaded.
}

function both_bool_vars_check() {
    if( (bool_big_image_loaded=="true") && (bool_all_comments_loaded=="true") ) {
        console.log("Both images loaded set");

        if(already_closed == 0)
            document.querySelector("#loader_inside_modal_svg").style.display="none";

        window.cancelAnimationFrame(reqId);

        console.log("Closing Reqid for animation inside modal is (normal): ");
        console.log(reqId);

        //document.getElementById("imageModal") = placebo_image;
        var actual_image = document.getElementById("imageModal");
        actual_image.parentNode.replaceChild(placebo_image,actual_image);
        placebo_image.setAttribute("class","img-fluid");
        placebo_image.setAttribute("id","imageModal");
        placebo_image.style.display="inline-block";

        document.getElementById("imageModal").style.visibility="visible";
        document.getElementById("DetailsDivModal").style.visibility="visible";

        document.getElementById("DetailsDivModal").style.display="block";

        document.getElementById("hr-bw-fame-comment").style.visibility="visible";
        document.getElementById("commentsForm").style.visibility="visible";
        document.getElementById("comment_InputImageModal").value="";
        document.getElementById("hr-bw-fame-comment").style.visibility="visible";
        document.getElementById("commentsForm").style.visibility="visible";

        //Resizing logic
         document.getElementById("imageDivModal").style.height = document.getElementById("DetailsDivModal").clientHeight +"px";
        // console.log("**Image modal clientheight : "+document.getElementById("imageModal").clientHeight);
        console.log("imageDivModal style.height : "+document.getElementById("imageDivModal").style.height);
        console.log("**Details div modal clientheight : "+document.getElementById("DetailsDivModal").clientHeight);
        //document.getElementById("comment_InputImageModal").style.maxWidth = document.getElementById("DetailsDivModal").clientWidth-70;
        // console.log(document.getElementById("DetailsDivModal").clientWidth + " and " + document.getElementById("comment_InputImageModal").style.maxWidth );

        if((document.getElementById("DetailsDivModal").clientWidth) > 500)
            document.getElementById("comment_InputImageModal").style.width = (0.8) * (document.getElementById("DetailsDivModal").clientWidth) + "px";
        if((document.getElementById("DetailsDivModal").clientWidth) > 300)
            document.getElementById("comment_InputImageModal").style.width = (0.7) * (document.getElementById("DetailsDivModal").clientWidth) + "px";
        else if ((document.getElementById("DetailsDivModal").clientWidth)>200)
            document.getElementById("comment_InputImageModal").style.width = (0.6) * (document.getElementById("DetailsDivModal").clientWidth) + "px";
        else
            document.getElementById("comment_InputImageModal").style.width = (0.5) * (document.getElementById("DetailsDivModal").clientWidth) + "px";


        document.getElementById("commentDivModal").style.width = document.getElementById("commentsDisplay").clientWidth + "px";

        //Image has completely loaded. We can safely enable clicking again.
        global_image_click_disabled = false;

        //document.getElementById("comment_InputImageModal").style.width = Math.min( (0.7) * (document.getElementById("DetailsDivModal").clientWidth),document.getElementById("DetailsDivModal").clientWidth-50) + "px";

            // if(document.getElementById("DetailsDivModal").clientHeight==document.getElementById("imageModal").clientHeight)
        // {
        //     console.log("Helper removed");
        //     $("#imageDivModal_helper").remove();
        // } else {
        //     console.log("Helper not removed!");
        //     console.log("cause compared ");
        //     console.log("detailsdiv cleintheight: ");
        //     console.log(document.getElementById("DetailsDivModal").clientHeight);

        //     console.log("with imagemodal clientheight: ");
        //     console.log(document.getElementById("imageModal").clientHeight);
        // }
    }
}

function loadModal(itemRef,nurl) {
    //nurl is the url of the image - from firebase storage.
    //itemRef is the reference of the image that user clicked on - from the firebase storage.
    already_closed = 0;

    placebo_image = document.createElement("img");
    reqId = window.requestAnimationFrame(draw);
    document.getElementById("realnameModal").innerHTML="";
    document.getElementById("tagsModal").innerHTML="";
    document.querySelector("#commentsDisplay").innerHTML="";
    document.getElementById("upvotesModal").innerHTML="";
    document.getElementById("viewsModal").innerHTML="";
    document.getElementById("dateModal").innerHTML="";
    document.getElementById("DetailsDivModal").style.display="none";
    document.getElementById("hr-bw-fame-comment").style.visibility="hidden";
    document.getElementById("commentsForm").style.visibility="hidden";
    document.getElementById("comment_InputImageModal").value="";
    bool_big_image_loaded = "false";
    bool_all_comments_loaded = "false";

    console.log("Starting Reqid for animation inside modal is: ");
    console.log(reqId);

    console.log("Paramter 1, Itemref is:");
    console.log(itemRef);

    console.log("Parameter 2, nurl is:");
    console.log(nurl);

    itemRef.getMetadata().then(function(itemMetadata){
        firebase.database().ref('Image').once('value', function(snapshot) {
            snapshot.forEach(function(uniqueIDsnapshot){
                uniqueIDsnapshot.forEach(function(childSnapshot) {
                    var childKey = childSnapshot.key;
                    var childData = childSnapshot.val();
                    if( childData.name == itemMetadata.name) {
                        console.log("Image id is "+childKey);
                        console.log("Yes");
                        console.log("(1 of 2): ChildKey is");
                        console.log(childKey);

                        console.log("(2 of 2): ChildData is");
                        console.log(childData);

                        //ChildKey is photoId.
                        //ChildData is all info about the photo - name,realname, tags, date etc.
                        firebase.database().ref('Image/'+uniqueIDsnapshot.key+'/'+ childKey).once('value').then(function(snapshot) {
                            clickCounter(childKey);

                            placebo_image.style.display="none";
                            placebo_image.setAttribute("onload" , "image_loaded()");
                            placebo_image.src=nurl;

                            document.getElementById("realnameModal").innerHTML = snapshot.val().realname;
                            firebase.database().ref('comments/'+ childKey).once('value', function(snapshotcomment) {

                                num_curr_comment = 0;

                                console.log("Total number of Users that have commented "+snapshotcomment.numChildren());

                                if( (!snapshotcomment.exists() ) || snapshotcomment.numChildren() == 0) {
                                    console.log("No comments to load");
                                    bool_all_comments_loaded = "true";
                                    both_bool_vars_check(); //All comments have been loaded. Check both the variables now.
                                }

                                snapshotcomment.forEach(function(childSnapshotcomment) {
                                    num_curr_comment++;
                                    console.log("Increase numcurr_comment, current value" + num_curr_comment);
                                    //This loop goes through all the comments pushed by childSnapshotcomment - which is the uniqueId of one of the many users
                                    //that have commented on this image.
                                    childSnapshotcomment.forEach(function(childSnapshotcommentDetails) {
                                        console.log(childSnapshotcomment.key+" commented "+childSnapshotcommentDetails.val());

                                        firebase.database().ref('users/'+childSnapshotcomment.key).once('value', function(snapshotcommentuser) {

                                            if(!snapshotcommentuser.exists()) {
                                                //Important to check - because some user might have commented on an image
                                                // and later deleted his account - so his username would not be found.
                                                if(num_curr_comment == snapshotcomment.numChildren()) {
                                                    console.log("Current num_curr_comment is "+ num_curr_comment);
                                                    console.log("All comments loaded");
                                                    bool_all_comments_loaded = "true";
                                                    both_bool_vars_check(); //All comments have been loaded. Check both the variables now.
                                                }
                                            } else {
                                                console.log("User is "+snapshotcommentuser.child("username").val());
                                                var commentputdiv=document.createElement("div");
                                                var commentputdiv=document.createElement("div");
                                                var commentputUserName=document.createElement("p");
                                                var commentputcomment=document.createElement("p");
                                                var commentputbutton=document.createElement("button");

                                                commentputUserName.innerHTML=snapshotcommentuser.child("username").val();
                                                commentputcomment.innerHTML=childSnapshotcommentDetails.val();
                                                commentputbutton.innerHTML="×";
                                                commentputbutton.setAttribute("class", "deleteCommentButton");
                                                commentputbutton.style.position="relative";

                                                commentputbutton.onclick = function() {
                                                    if(global_LoggedIn==1)
                                                    {
                                                        firebase.database().ref('comments/'+childKey+'/'+childSnapshotcomment.key+'/'+childSnapshotcommentDetails.key).remove();
                                                        commentputdiv.style.display="none";
                                                        console.log("Comment Deleted");
                                                    }
                                                }

                                                commentputUserName.style.fontWeight="bold";
                                                commentputUserName.style.float="left";
                                                commentputcomment.style.float="left";
                                                commentputcomment.style.marginLeft="3px";
                                                commentputdiv.style.clear="both";

                                                commentputdiv.appendChild(commentputUserName);
                                                commentputdiv.appendChild(commentputcomment);
                                                if(global_LoggedIn==1)
                                                {
                                                    if(snapshotcommentuser.key==firebase.auth().currentUser.uid)
                                                    commentputdiv.appendChild(commentputbutton);
                                                }

                                                document.querySelector("#commentsDisplay").appendChild(commentputdiv);

                                                if(num_curr_comment == snapshotcomment.numChildren()) {
                                                    console.log("Current num_curr_comment is "+ num_curr_comment);
                                                    console.log("All comments loaded");
                                                    bool_all_comments_loaded = "true";
                                                    both_bool_vars_check(); //All comments have been loaded. Check both the variables now.
                                                }
                                            }
                                        });
                                    });
                                });
                            });

                            snapshot.val().tags.forEach(function(item) {

                                var tag_item = document.createElement("p");
                                tag_item.setAttribute("id","tag-item");
                                tag_item.innerHTML="#"+item;
                                document.getElementById("tagsModal").appendChild(tag_item);
                            });

                            view_icon = document.createElement("span");
                            view_icon.setAttribute("class","material-icons-two-tone");
                            view_icon.innerHTML="visibility";
                            view_icon.style.float="left";


                            view_num=document.createElement("p");
                            view_num.style.float="left";
                            view_num.style.marginLeft="5px";

                            var countViews = 0;
                            var countUsers = 0;

                            firebase.database().ref('Views/'+ childKey).once('value', function(SnapshotUserListInsideViewsDB) {
                                if(!SnapshotUserListInsideViewsDB.exists()) {
                                    view_num.innerHTML = 0;
                                    return;
                                }

                                SnapshotUserListInsideViewsDB.forEach(function(SnapshotSingleUserInsideViewsDB){

                                    countUsers++;

                                    if(0 < SnapshotSingleUserInsideViewsDB.val() < 100 && !isNaN(SnapshotSingleUserInsideViewsDB.val()) )
                                    countViews += SnapshotSingleUserInsideViewsDB.val();

                                    if(countUsers == SnapshotUserListInsideViewsDB.numChildren()) {
                                        view_num.innerHTML = countViews;
                                        return;
                                    }
                                });
                            });

                            if(global_LoggedIn==1)
                                document.getElementById("viewsModal").appendChild(view_icon);
                            document.getElementById("viewsModal").appendChild(view_num);

                            upvote_icon = document.createElement("span");
                            upvote_icon.setAttribute("class","material-icons-outlined");
                            upvote_icon.innerHTML="thumb_up";
                            upvote_icon.style.float="left";


                            upvote_num = document.createElement("p");
                            upvote_num.style.float = "left";
                            upvote_num.style.marginLeft = "5px";
                            upvote_num.style.float="left";

                            firebase.database().ref('Upvotes/'+ childKey).once('value', function(upvoteSnapshot) {
                                if(upvoteSnapshot.exists())
                                    upvote_num.innerHTML=upvoteSnapshot.numChildren();
                                else
                                    upvote_num.innerHTML=0;
                                if(global_LoggedIn==1)
                                    document.getElementById("upvotesModal").appendChild(upvote_icon);
                                document.getElementById("upvotesModal").appendChild(upvote_num);
                            })

                            var d = new Date();
                            var imgdate=snapshot.val().date;

                            if(snapshot.val().date) {
                                var imgday  = imgdate.day;
                                var imgmonth  = imgdate.month;
                                var imgyear  = imgdate.year;

                                var currday = d.getDate();
                                var currmonth = d.getMonth()+1;
                                var curryear = d.getFullYear();

                                var imgmin  = imgdate.min;
                                var imghour  = imgdate.hour;

                                var currmin = d.getMinutes();
                                var currhour = d.getHours();

                                var datetext="";

                                if(curryear==imgyear){
                                    if(currmonth==imgmonth){
                                        if(currday==imgday){
                                            if(currhour==imghour){
                                                if(currmin==imgmin){
                                                    datetext="Just Now";
                                                }
                                                else datetext=(currmin-imgmin).toString()+"min";
                                            }
                                            else datetext=(currhour-imghour).toString()+"hrs";
                                        }
                                        else datetext=(currday-imgday).toString()+"d";
                                    }
                                    else{
                                        if(currday<imgday) {
                                            datetext = ( (((currmonth-imgmonth - 1)*4) + Math.floor((30+currday-imgday)/7)).toString() ) +"w";

                                            if(datetext=="0w")
                                                datetext = (30+currday-imgday).toString() + "d"; //Special Case: Ex. to test: Currday in first week of a month, imgday on last week of the previous month.
                                        }
                                        else
                                            datetext= ( (((currmonth-imgmonth)*4) + Math.floor((currday-imgday)/7)).toString() ) +"w";
                                    }
                                }
                                else {
                                    if( ( (curryear-imgyear) ==1) && currmonth<imgmonth)
                                        datetext= ( (((12 + currmonth-imgmonth)*4) + Math.floor((currday-imgday)/7) ).toString() ) +"w";
                                    else if ((curryear-imgyear==1))
                                        datetext= ( (12 + ((currmonth-imgmonth)*4) + Math.floor((currday-imgday)/7) ).toString() ) +"w";
                                    else
                                        datetext=((curryear-imgyear)).toString()+"yrs";
                                }

                                document.getElementById("dateModal").innerHTML="";
                                document.getElementById("dateModal").innerHTML = datetext;

                            }
                            else {
                                document.getElementById("dateModal").innerHTML="";
                                document.getElementById("dateModal").innerHTML = "Date Unavailable";
                            }

                            document.getElementById("commentButton").onclick = function() {
                                //Only input a comment if a user is logged in
                                if(global_LoggedIn==1) {
                                    const comment=document.getElementById("comment_InputImageModal").value;
                                    var commentNodeRef = firebase.database().ref('images/' + childKey + '/comments');
                                    var commentRef = commentNodeRef.push().key;
                                    var currentUser= firebase.auth().currentUser;
                                    var updates = {};
                                    updates['images/' + childKey + '/comments/'+commentRef+'/'+currentUser.uid] = comment;
                                    document.getElementById("comment_InputImageModal").value="";

                                    if(comment=="") {
                                        console.log("Empty comment. Not added.");
                                        return;
                                    }
                                    console.log("Comment Added");

                                    var newCommentKey = firebase.database().ref().child('comments/'+childKey+currentUser.uid).push().key;
                                    var updates = {};
                                    updates['/comments/'+childKey+'/'+currentUser.uid+'/'+newCommentKey] = comment;

                                    firebase.database().ref('users/'+currentUser.uid+'/username').once('value', function(childSnapshotcommentuser) {
                                        console.log("Hello");
                                        document.getElementById("hr-bw-fame-comment").style.visibility="visible";
                                        document.getElementById("commentsForm").style.visibility="visible";

                                        //Show the comment in the current box that is opened as well
                                        var commentputdiv=document.createElement("div");
                                        var commentputUserName=document.createElement("p");
                                        var commentputcomment=document.createElement("p");
                                        var commentputbutton=document.createElement("button");
                                        commentputbutton.setAttribute("class", "deleteCommentButton");
                                        commentputbutton.innerHTML="×";

                                        commentputbutton.onclick = function() {
                                            console.log("Child key is ");
                                            console.log(childKey);
                                            console.log("Current user uid is ");
                                            console.log(currentUser.uid);
                                            console.log("newcommentkey is ");
                                            console.log(newCommentKey);
                                            firebase.database().ref('comments/'+childKey+'/'+currentUser.uid+'/'+newCommentKey).remove();
                                            commentputdiv.style.display="none";
                                            console.log("Comment Deleted");
                                        }

                                        commentputUserName.innerHTML=childSnapshotcommentuser.val();
                                        commentputcomment.innerHTML=comment;

                                        commentputUserName.style.fontWeight="bold";
                                        commentputUserName.style.float="left";
                                        commentputcomment.style.float="left";
                                        commentputcomment.style.marginLeft="3px";
                                        commentputdiv.style.clear="both";

                                        commentputdiv.appendChild(commentputUserName);
                                        commentputdiv.appendChild(commentputcomment);
                                        commentputdiv.appendChild(commentputbutton);
                                        document.querySelector("#commentsDisplay").appendChild(commentputdiv);

                                        console.log("New comment added - calling both bool check");
                                        both_bool_vars_check(); //To resize according to new size of modal.

                                            //document.getElementById("imageModal").style.height=document.getElementById("DetailsDivModal").clientHeight;

                                    });

                                    return firebase.database().ref().update(updates);
                                } else {
                                    document.querySelector("body").style.overflow = 'hidden'; //To hide background scroll.
                                    $("#LogIn_Modal").modal();
                                }
                            }
                        });
                    }
                });
            })

     });
    });
}

//to count the number of times an image is opened. Invoked when modale is opened
function clickCounter(childKey) {
    if(global_LoggedIn == 1) {
        var sessionStorage_key = "Key"+childKey+'_'+firebase.auth().currentUser.uid;

        if (typeof(Storage) !== "undefined") {
            sessionStorage.setItem(sessionStorage_key, sessionStorage.getItem(sessionStorage_key)+1);
        } else {
        console.log("Sorry, your browser does not support web storage...")
        }

        if(sessionStorage.getItem(sessionStorage_key).length==1) {
            var updates = {};

            var currViews = 0;

            firebase.database().ref('Views/' + childKey + '/' + firebase.auth().currentUser.uid).once('value', function(snapshot){
                if(snapshot.exists()) {
                    console.log("##View exists for this user with\n key:");
                    console.log(snapshot.key);

                    console.log("and view: ");
                    console.log(snapshot.val());

                    updates['Views/' + childKey + '/' + firebase.auth().currentUser.uid] = 1 + snapshot.val();
                    console.log("#View increased by 1");
                    return firebase.database().ref().update(updates);

                } else {
                    updates['Views/' + childKey + '/' + firebase.auth().currentUser.uid] = 1;
                    console.log("##View added. Initialized to 1;")
                    return firebase.database().ref().update(updates);
                }
            });

        } else {
            console.log("##This user has already viewed this image in this session. So not updating view field.");
        }
    }
}

$("#commentsForm").submit(function(e) {
    e.preventDefault();
})


function deleteComment()
{
    console.log("Comment Deleted")
}

var commentAddTextField=document.getElementById("comment_InputImageModal");

commentAddTextField.addEventListener('keypress', function(event){
    if(event.code=="Enter") {
        event.preventDefault(); //Default action was to print the first emoji

        document.getElementById("commentButton").click(); //Click the submit button instead.
    }
});