var src_img_list=[];
var bool_my_img="false";

var src_img;

var last_loaded = -1;
var iterator_grad_id_img=0;
var iterator_imgcont_id_img=0;
var iterator_img_id_img=0;
var bool_user_has_img = 0;

//This variable is used to count the number of images
//uploaded by the currently logged in user when he click on 'my images'
//This counter helps in showing "no images uploaded" when the last image is deleted
var number_of_my_images = 0;

function set_loaders_view_switched() {
	document.getElementById('btn-sort-dropdown').setAttribute("class","btn btn-dark");
	document.getElementById('spinner-sort-button').style.display="block";
	document.getElementById('spinner-sort-button').classList.toggle("visible"); //Should get added.
	document.getElementById('btn-sort-dropdown').setAttributeNode(document.createAttribute("disabled"));

	//Whenever view is switched, clear the no image uploaded sign.
	document.getElementById('no_image_user_has_img').style.display="none";

	document.getElementById('myImg_button').setAttributeNode(document.createAttribute("disabled"));
	document.getElementById('allImg_button').setAttributeNode(document.createAttribute("disabled"));
}

function switch_div_button_enter_my_images() {
	if(!document.getElementById('myImg_button').hasAttribute("disabled")) {
		document.getElementById('myImg_button').style.cursor="pointer";
		document.getElementById('myImg_button').style.backgroundColor = "Orange";
		document.getElementById('myImg_button').style.color = "White";
	} else {
		document.getElementById('myImg_button').style.cursor="default";
	}
}

function switch_div_button_enter_all_images() {
	if(!document.getElementById('allImg_button').hasAttribute("disabled")) {
		document.getElementById('allImg_button').style.cursor="pointer";
		document.getElementById('allImg_button').style.backgroundColor = "Orange";
		document.getElementById('allImg_button').style.color = "White";
	} else {
		document.getElementById('allImg_button').style.cursor="default";
	}
}


function switch_div_button_leave_my_images() {
	if(bool_my_img == "true" && !document.getElementById('myImg_button').hasAttribute("disabled")) {
		document.getElementById('myImg_button').style.backgroundColor = "Orange";
		document.getElementById('myImg_button').style.color = "Black";
	} 
	else if(!document.getElementById('myImg_button').hasAttribute("disabled")){
		document.getElementById('myImg_button').style.backgroundColor = "White";
		document.getElementById('myImg_button').style.color = "Black";
	}
}

function switch_div_button_leave_all_images() {
	if(bool_my_img == "false" && !document.getElementById('myImg_button').hasAttribute("disabled")) {
		document.getElementById('allImg_button').style.backgroundColor = "Orange";
		document.getElementById('allImg_button').style.color = "Black";
	} 
	else if(!document.getElementById('allImg_button').hasAttribute("disabled")){
		document.getElementById('allImg_button').style.backgroundColor = "White";
		document.getElementById('allImg_button').style.color = "Black";
	}
}


function myImages() {
	var allPhotos=document.getElementById("header");

	document.querySelector("#btn-sort").innerHTML="Random";

	document.getElementById('allImg_button').style.backgroundColor = "White";
	document.getElementById('myImg_button').style.backgroundColor = "Orange";
	document.getElementById('myImg_button').style.color = "Black";
	src_img_list=[];
    while (allPhotos.firstChild) {
		allPhotos.removeChild(allPhotos.lastChild);
	}

	set_loaders_view_switched();

	bool_my_img="true";
	last_loaded = -1;
    Photos();
}


function loaded_an_image(imgprop) {
	var num =  imgprop.target.id; //Correct way to get id - supported in both chrome/firefox.

	//Remove the gradient corresponding to the image
	var remgrad=document.querySelector("#grad-"+(last_loaded+2));
	remgrad.remove();
	last_loaded++;

	//Num of images to be loaded now known. Safe to remove global first loader and show index content
	var nav_bar;
	nav_bar = document.getElementById("navbar_index_top");
	if(nav_bar) {
		nav_bar.style.visibility="visible";
	}

	if(document.getElementById("loader_global_first")) {
		document.getElementById("loader_global_first").remove();
		document.getElementById("loader_global_first_facts").remove();
		//When the Global Loader stops loading, the wavy header will appear
		if(global_LoggedIn!=1)
		{
			document.getElementById('sort_button_div_index').style.display="block";
			document.getElementById('top_wavy_header').style.display="block";
		}
	}
	if(global_LoggedIn!=1 && last_loaded > 5)
		document.getElementById('footer_svg').style.display="block";

	if(document.getElementById("loader-loggedin-all-modals")) {
		//console.log("HIii");
		document.getElementById("loader-loggedin-all-modals").remove();
	}

	document.getElementById("header").style.display="flex";

	if(num==iterator_grad_id_img) { //Runs when all the images have been loaded.
		document.getElementById('btn-sort-dropdown').setAttribute("class","btn btn-dark dropdown-toggle");
		document.getElementById('spinner-sort-button').style.display="none";
		document.getElementById('spinner-sort-button').classList.toggle("visible"); //Should get removed.
		document.getElementById('btn-sort-dropdown').removeAttribute("disabled");

		if(document.getElementById("footer_svg")) {
			document.getElementById("footer_svg").style.bottom=0;
		}

		if(global_LoggedIn==1)
		{
			document.getElementById('myImg_button').removeAttribute("disabled");
			document.getElementById('allImg_button').removeAttribute("disabled");
		}
	}

	var imgcont = document.querySelector("#imgcont-"+num);
	imgcont.style.display="block";
	src_img= document.getElementById("header");
	src_img.insertBefore(imgcont,src_img.children[last_loaded]);
}

function allImages() {
	src_img_list=[];
	bool_my_img="false";
	document.querySelector("#btn-sort").innerHTML="Random";
    var allPhotos=document.getElementById("header");

	document.getElementById('allImg_button').style.backgroundColor = "Orange";
	document.getElementById('myImg_button').style.backgroundColor = "White";
	document.getElementById('allImg_button').style.color = "Black";


    while (allPhotos.firstChild) {
		allPhotos.removeChild(allPhotos.lastChild);
	}

	set_loaders_view_switched();

	bool_my_img="false";
	last_loaded = -1;
    Photos();
}

function check_for_last_img_no_img_uploaded(img_counter, total_images) {
	//This prints which image called this function (this image is uploaded on the server,
	//but is not uploaded by the current user, and the current user has
	//asked to see 'My Images' section)
	//console.log(img_counter + " and " + total_images + " and bool value is "+bool_user_has_img);

	if(img_counter == total_images) {
		//This means we have checked all the images so far and the last one is not uploaded by the user.
		//Lets check for the remaining ones using the bool variable.

		if(bool_user_has_img == 0) {
			//No image uploaded by user so far. Show the message.
			document.getElementById('no_image_user_has_img').style.display="block";
			document.getElementById('btn-sort-dropdown').setAttribute("class","btn btn-dark dropdown-toggle");
			document.getElementById('spinner-sort-button').style.display="none";
			document.getElementById('spinner-sort-button').classList.toggle("visible"); //Should get removed

			//Do not enable sorting since user has not uploaded any image.
			//document.getElementById('btn-sort-dropdown').removeAttribute("disabled");

			document.getElementById('myImg_button').removeAttribute("disabled");
			document.getElementById('allImg_button').removeAttribute("disabled");
		}

	}

}

function Photos(){
	console.log("Called Photos");
	iterator_grad_id_img=0;
	iterator_imgcont_id_img=0;
	iterator_img_id_img=0;
	src_img = document.getElementById("header");

	var storage_img = firebase.storage();
	var storageRef_img = storage_img.ref();
	var image_counter_no_img_check = 0;
	bool_user_has_img = 0;

	//This variable is used to count the number of images
	//uploaded by the currently logged in user when he click on 'my images'
	//This counter helps in showing "no images uploaded" when the last image is deleted
	number_of_my_images = 0;

	src_img_list.length=0;

	// Create a reference under which you want to list
	var listRef_img = storageRef_img.child('images');

	listRef_img.listAll().then(function(res) {

		//console.log(res.items.length);
		res.items.forEach(function(itemRef) {

			var img_full_name=itemRef.name;
			var img_pushed_key;
			var users= firebase.auth().currentUser;
			var userInImageName = "NULL";
			image_counter_no_img_check++;

			if(global_LoggedIn == 1)
				userInImageName = img_full_name.search(users.uid);

			var realname;

			//This is to check if this image has to be shown
			//So checks if the user has uploaded this image (in case of showing myimages)
			//Logic: Image name is already appended with userid of owner while uploading.
			if(global_LoggedIn==1 && userInImageName==-1 && bool_my_img=="true") {
				check_for_last_img_no_img_uploaded(image_counter_no_img_check, res.items.length);

				//return since we do not want to process any thing more about this image
				//since this image is not uploaded by the current user and the current user is looking
				//for 'My Images' only.
				return;
			} else if (global_LoggedIn==1 && userInImageName!=1 && bool_my_img=="true") {
				bool_user_has_img = 1;
				number_of_my_images++;
			}

			//console.log("Loading an image");
			//------------------------------------------Start Gradient Work----------------------------------------//
			iterator_grad_id_img++;

			var graddiv = document.createElement("div");

			graddiv.setAttribute("class","grad-div");
			graddiv.setAttribute("id","grad-"+iterator_grad_id_img);
			//	var colors is already defined in gradcolors.js
			graddiv.style.backgroundImage = "linear-gradient(to bottom right, "+colors[Math.floor((Math.random() * colors.length) )]+","+colors[Math.floor((Math.random() * colors.length) )]+")";

			src_img.appendChild(graddiv);
			//------------------------------------------End Gradient Work----------------------------------------//

			//------------------------------------------Start Get Images Detail----------------------------------------//
			itemRef.getDownloadURL().then(function(nurl) {

				var img_author;

				iterator_img_id_img++;

				var imgdiv=document.createElement("div");
				imgdiv.setAttribute("class","div-img-parent");

				var img = document.createElement("img");
				img.setAttribute("class", "img-load");

				img.setAttribute("id",iterator_img_id_img);

				//img.setAttribute("onload", "loaded_an_image(event)");
				img.addEventListener('load', (event)=>{
					loaded_an_image(event);
				});
				img.src = nurl;

				var textdiv=document.createElement("div");
				textdiv.setAttribute("class","text-block");

				var mini_loader_inside_div_prog = document.createElement("div");
				mini_loader_inside_div_prog.setAttribute("class","progress");

				var mini_loader_inside_div_color = document.createElement("div");
				mini_loader_inside_div_color.setAttribute("class","color");

				mini_loader_inside_div_prog.appendChild(mini_loader_inside_div_color);

				var mini_loader_inside_div_prog_2 = document.createElement("div");
				mini_loader_inside_div_prog_2.setAttribute("class","progress2");

				var mini_loader_inside_div_color_2 = document.createElement("div");
				mini_loader_inside_div_color_2.setAttribute("class","color2");

				mini_loader_inside_div_prog_2.appendChild(mini_loader_inside_div_color_2);

				var textdiv_forUpvote=document.createElement("div");
				textdiv_forUpvote.setAttribute("class","text-block");
				var views_para=document.createElement("p");
				var upvotes_para=document.createElement("p");
				var name_para=document.createElement("p");
				name_para.setAttribute("class", "text_div_username_para");
				var name_photo_real =document.createElement("p");
				name_photo_real.setAttribute("class", "photo_name_real");
				// "nowrap" property prevents the contents of div from going to the next line
				name_photo_real.style.whiteSpace="nowrap";

				var hidden_upvotes = document.createElement("p");
				hidden_upvotes.style.display='none';

				var hidden_views = document.createElement("p");
				hidden_views.style.display='none';


				var hidden_date = document.createElement("p");
				hidden_date.style.display='none';

				textdiv.appendChild(mini_loader_inside_div_prog);
				textdiv.appendChild(mini_loader_inside_div_prog_2);

				itemRef.getMetadata().then(function(itemMetadata){

					firebase.database().ref('Image').once('value', function(snapshot) {
						snapshot.forEach(function(uniqueIDsnapshot){
							uniqueIDsnapshot.forEach(function(childSnapshot) {
								// console.log(childSnapshot.val().name)
								// console.log(itemMetadata.name)
								var childKey = childSnapshot.key;
								var childData = childSnapshot.val();

								if( childData.name == itemMetadata.name)
								{
									firebase.database().ref('Image/'+uniqueIDsnapshot.key+'/'+ childKey).once('value').then(function(snapshot) {
										//initializing the tooltip
										$(function () {
											$('[data-toggle="tooltip"]').tooltip()
										})

										img_pushed_key = childKey;
										realname = snapshot.val().realname;
										views_para.innerHTML="";
										views_para.innerHTML="";
										upvotes_para.innerHTML="";
										upvotes_para.innerHTML="Upvotes: "+snapshot.val().upvotes;
										name_photo_real.innerHTML="";

										//the html attributes are set to the p element of the tooltip
										name_photo_real.setAttribute("data-toggle", "tooltip");
										name_photo_real.setAttribute("data-placement", "bottom");
										name_photo_real.title=snapshot.val().realname;
										mini_loader_inside_div_prog.remove();

										name_photo_real.innerHTML=snapshot.val().realname;

										//Get Upvotes
										firebase.database().ref('Upvotes/'+ childSnapshot.key).once('value', function(upvoteSnapshot) {
											console.log("Tried to fetch upvote");
											if(upvoteSnapshot.exists())
												hidden_upvotes.innerHTML=upvoteSnapshot.numChildren();
											else
												hidden_upvotes.innerHTML=0;
										})


										///-------------------------------START: Code for getting views. -------------------------------

										var countViews = 0;
										var countUsers = 0;

										firebase.database().ref('Views/'+ childKey).once('value', function(SnapshotUserListInsideViewsDB) {
											if(!SnapshotUserListInsideViewsDB.exists()) {
												hidden_views.innerHTML = 0;
												return;
											}

											SnapshotUserListInsideViewsDB.forEach(function(SnapshotSingleUserInsideViewsDB){

												countUsers++;

												if(0 < SnapshotSingleUserInsideViewsDB.val() < 100 && !isNaN(SnapshotSingleUserInsideViewsDB.val()))
												countViews += SnapshotSingleUserInsideViewsDB.val();

												if(countUsers == SnapshotUserListInsideViewsDB.numChildren()) {
													hidden_views.innerHTML = countViews;
													return;
												}
											});
										});

										///-------------------------------END: Code for getting views.-------------------------------

										hidden_date.innerHTML="NULL";

										if(snapshot.val().date) {
											hidden_date.innerHTML=snapshot.val().date.year;
											hidden_date.innerHTML+=snapshot.val().date.month;
											hidden_date.innerHTML+=snapshot.val().date.day;
											hidden_date.innerHTML+=snapshot.val().date.hour;
											hidden_date.innerHTML+=snapshot.val().date.min;
											hidden_date.innerHTML+=snapshot.val().date.sec;
										}
									});

									firebase.database().ref('/users/' + uniqueIDsnapshot.key).once('value').then(function(snapshot) {
										name_para.innerHTML="";
										name_para.innerHTML=snapshot.val().username;
										mini_loader_inside_div_prog_2.remove();
										img_author=snapshot.val().username;
									});
									
									textdiv.appendChild(name_photo_real);
									
									textdiv.appendChild(name_para);

									//Upvote Function
									upvote_btn.onclick = function() {
										if(global_LoggedIn==0) {
											document.querySelector("body").style.overflow = 'hidden'; //To hide background scroll.
											$("#LogIn_Modal").modal();
										} else {
											upvote(img_full_name, users, views_para, upvotes_para, name_para, textdiv, upvote_btn, img_author, childKey);
										}
									}
									//it checks whether a particular image is upvoted or unvoted by the signed in user
									if(global_LoggedIn == 1) {
										const Upvote_user = firebase.auth().currentUser;
										//This function checks on logged in page par check karta hai yeh kon-kon si photo upvoted hai
										firebase.database().ref('Upvotes/' + childKey+'/'+Upvote_user.uid).once('value', function(Upvote_Snapshot) {
											if(Upvote_Snapshot.exists())
												upvote_btn.style.backgroundPosition='0px 2px';
											else
												upvote_btn.style.backgroundPosition='0px -25px';
										});
									} else {
										upvote_btn.style.backgroundPosition='0px -25px';
									}

								//Temporary solution for sorting. To be removed later.
									textdiv.appendChild(hidden_upvotes);
									textdiv.appendChild(hidden_views);
									textdiv.appendChild(hidden_date);

								}
							});
						})

					});
				});

				//------------------------------------------End Get Images Details----------------------------------------//

				var imgcont=document.createElement("div");
				imgcont.setAttribute("class", "div-img-load");

				iterator_imgcont_id_img++;
				imgcont.setAttribute("id", "imgcont-"+iterator_imgcont_id_img);

				if(global_LoggedIn==1 & userInImageName!=-1 && bool_my_img=="true") {

					var delBtn_div = document.createElement("div");
					delBtn_div.setAttribute("class","delBtn-div");

					var delBtn_modal_delete_btn=document.createElement("button");
					delBtn_modal_delete_btn.setAttribute("class", "common_button_scheme");
					delBtn_modal_delete_btn.innerHTML="Delete";

					var delBtn_modal_initiator = document.createElement("button");
					delBtn_modal_initiator.setAttribute("class", "delBtn_modal_initiator");
					delBtn_modal_initiator.innerHTML='×';

					imgdiv.appendChild(delBtn_modal_initiator);
					imgcont.appendChild(delBtn_div);
				}

				var upvote_btn=document.createElement("button");
				//upvote_btn.innerHTML="Upvote";

				upvote_btn.setAttribute("class", "upvote_btn");
				upvote_btn.style.display='block';

				//Load the un-voted wala icon in the beginning, then decide.
				upvote_btn.style.backgroundPosition='0px -25px';

				if(global_LoggedIn == 1) {
					//This function checks on logged in page par check karta hai yeh kon-kon si photo upvoted hai
					firebase.database().ref('/images').once('value', function(snapshot) {
						snapshot.forEach(function(childSnapshot) {
							var childKey = childSnapshot.key;
							var childData = childSnapshot.val();
							if(childSnapshot.val().name == img_full_name) {
								var counter=0;
								if(childSnapshot.val().upvotes!=0) {
									if(global_LoggedIn==1 && childSnapshot.val().upvoter[users.uid]==users.uid) {
										counter++;
									}
								}
								if(counter==0) {
									upvote_btn.style.backgroundPosition='0px -25px';
								}
								else {
									upvote_btn.style.backgroundPosition='0px 0px';
								}
							}
						});
					});
				} else {
					upvote_btn.style.backgroundPosition='0px -25px';
				}



				img.onclick = function() {

					if(global_image_click_disabled) {
						alert("You're going quite fast. Calm down please :)");
						return;
					}

					global_image_click_disabled = true;
					document.querySelector(".modal-dialog-image").style.display = 'block';
					document.querySelector("body").style.overflow = 'hidden';

					document.getElementById("realnameModal").innerHTML="";
					document.getElementById("tagsModal").innerHTML="";
					document.getElementById("upvotesModal").innerHTML="";
					document.getElementById("imageModal").style.visibility="hidden";
					//When the modal is opened, restrict the maximum height and width - to show loader properly and avoid weird long resizing for a second error.
					//document.querySelector(".modal-dialog-image").style.maxHeight="600px";
					document.querySelector(".modal-dialog-image").style.maxWidth="1200px";
					document.getElementById("hr-bw-fame-comment").style.visibility="hidden";

					$('.btn-close-image').css('display','block');
					$('.modale').addClass('opened');
					loadModal(itemRef,nurl);
				}

				//Upvote function
				upvote_btn.onclick = function() {
					if(global_LoggedIn==0) {
						document.querySelector("body").style.overflow = 'hidden'; //To hide background scroll.
						$("#LogIn_Modal").modal();
					} else {
						upvote(img_full_name, users, views_para, upvotes_para, name_para, textdiv, upvote_btn, img_author);
					}
				}

				imgdiv.appendChild(img);
				imgcont.appendChild(imgdiv);
				textdiv.prepend(upvote_btn);
				imgcont.appendChild(textdiv);

				imgcont.style.display="none";
				src_img.appendChild(imgcont);

				src_img_list.push(imgcont);

				delBtn_modal_initiator.onclick = function() {
					document.getElementById("del_ind_footer").innerHTML=" "
					document.getElementById("del_ind_footer").appendChild(delBtn_modal_delete_btn);
					document.querySelector("#Del_single_image").innerHTML ="";
					document.querySelector("#Del_single_image").innerHTML ="Are you sure you want to delete the post "+ realname +"? This action cannot be reverted";
					$("#modalDelete_Individual").modal();
				};
				delBtn_modal_delete_btn.onclick = function() {
					console.log("Delete Button clicked")
					delete_image_individual(storageRef_img, img_full_name, imgcont, firebase.auth().currentUser.uid);
					$("#modalDelete_Individual").modal('hide');
					number_of_my_images--;

					if(number_of_my_images == 0) {
						document.getElementById('no_image_user_has_img').style.display="block";
					}
				};

			}).catch(function(error) {

					// A full list of error codes is available at
					// https://firebase.google.com/docs/storage/web/handle-errors
					switch (error.code) {
						case 'storage/object-not-found':
						// File doesn't exist
						break;

						case 'storage/unauthorized':
						// User doesn't have permission to access the object
						break;

						case 'storage/canceled':
						// User canceled the upload
						break;

						case 'storage/unknown':
						// Unknown error occurred, inspect the server response
						break;
					}
			});
	//------------------------------------------------------End Get Images Details-------------------------------------------
		});
	}).catch(function(error) {
		console.log(error);
	});
}