function add_username_to_registeredusername_database(updates){//Add the updates to Allregisteredusername  
  return 
}

function redirect_to_login_page() {
  var path = window.location.href;
  var split = path.split("/");
  var x = split.slice(0, split.length - 1).join("/") + "/";

  console.log("To be replaced with : " + x + "loggedin.html");
  window.location.replace(x + "loggedin.html");
}

function register_new_user_database(user, uName) {
  console.log("User signed up just now. Lets update the database and user profile.");
  var updates = {};

  //Updates to be added to Allregisteredusername
  updates['AllRegisteredUsernames_to_UserId/' + uName] = user.uid;
  
  //Add to users database - entry: username and email
  firebase.database().ref('users/' + user.uid).set({
    username: uName,
    email: user.email,
    }, function (error) {
      if (error) {
        alert("Failed to write in 'users' database. Please contact admin@awesomeus.tech");
        console.log("//The write failed");
      } else {
        console.log("//Data saved successfully");
        //Once user is added to database 'users' with username and email
        //Then add to registeredusername_to_userid

          firebase.database().ref().update(updates,(error) => {
            if (error) {
              alert("Failed to write in 'registeredusername_to_userid' database. Please contact admin@awesomeus.tech");
              // The write failed...
            } else {
              //Once username is added to database 'registeredusername_to_userid' with username and userid
              //Then add to update profile

              user.updateProfile({
                displayName: uName
              }).then(() => {
                // Update successful
                //So at this point, the database has been updated and the userprofile has been updated as well. 
                //We can safely reroute the url. 
                redirect_to_login_page();
                // ...
              }).catch((error) => {
                // An error occurred
                alert("Error in updating the user profile. Please contact admin@awesomeus.tech")
                // ...
              });  
            }
          });
        }
      }
  );
}

//for checking the status of the user i.e whether he is logged in or not
auth.onAuthStateChanged(user => {
  if (user) {
   global_LoggedIn=1;
   console.log('User Logged In: ', user);

   console.log("Opened: " + window.location.href);

   if(global_index_signed_up == 1) {
      register_new_user_database(user, global_index_signed_up_username);
   }else if (global_google_sign_inorup == 1) {
      //Check whether Google Sign in or sign up. If sign up, ask for setting up a username

      console.log("Checking whether Google Sign in or sign up.");

      var counter_user = 0; //This counter is to check when we have reached the last user. 
      //If we reach the last user and still have not matched the email with any user so far
      //It means this user is not registered. We need to register the user - ask for username first thou.

      var total_users;
      var email_matched;

      firebase.database().ref('users').once('value', function(snapshot) {
        //Check if any user already exists in database with the provided email.
        //If yes, directly login.
        //Otherwise ask for setting up a new unique username.

        console.log("Checking if email already exists");

        console.log("From ")
        console.log(snapshot);
        total_users =  snapshot.numChildren();
        email_matched = 0;

        snapshot.forEach(function(childSnapshot) {

          counter_user++;

          var childKey = childSnapshot.key;
          var childData = childSnapshot.val();

          console.log("Checked "+counter_user+" out of " +total_users + "emails");

          if(childSnapshot.val().email == user.email) {
            console.log("User already exists in the database. Proceed to log in");
            redirect_to_login_page();
            email_matched = 1;
          } else if(total_users == counter_user && email_matched == 0) {
            //Reached the last user. Proceed to signing up a new user. Ask for a user name first.    
            console.log("Email did not match. Proceed to ask for a username");


            //Hide the login modal if it is currently visible, else hide the signup modal.
            if(document.getElementById('LogIn_Modal').classList.contains('show')) {
              document.getElementById("LogIn_Modal").classList.remove("fade");
              $('#LogIn_Modal').modal('hide');
            }
            else {
              document.getElementById("SignUp_Modal").classList.remove("fade");
              $('#SignUp_Modal').modal('hide');
            }

            $('#google_select_username_modal').modal('show');

            // document.getElementById("modal-title-log-in").innerHTML="Sign Up";
            // document.getElementById('sign_up_modal_header_spinner').style.display="none";
            // document.getElementById("google_signup_username").style.display="block";

            // document.getElementById('small-text-login-modal').style.display="none";
            // document.getElementById('login-email').style.display="none";
            // document.getElementById('login-password').style.display="none";
            // document.getElementById('forgot-password-hyperlink-login-modal').style.display="none";
            // document.getElementById('sign-up-instead-login-modal').style.display="none";
            // document.getElementById("sign_in_with_google_div").style.display="none";

            // document.getElementById('log_in_button_index').style.display="block";
            // document.getElementById('login_text_inside_button_index').innerHTML="Sign Up";  
            // //After this user enters a username and submits the login form.
          }
        })
      });
      
    } else{
      //Coming here means user did not sign up right now. He is logged in from the last time. Or he logged in right now. 
      redirect_to_login_page();
    }
  } else {
   global_LoggedIn=0;
   console.log('User Logged Out');
  }
});


//Disable scroll when login/signup modal is opened
function open_modal_login_signup(){
  document.querySelector("body").style.overflow = 'hidden';
}

$('#SignUp_Modal').on('hidden.bs.modal', function () {
  if(!$('.modale').hasClass('opened'))
    document.querySelector("body").style.overflow = 'visible'; ///Ye background scroll wapis enable krne ke lie hai.
})


$('#LogIn_Modal').on('hidden.bs.modal', function () {
  if(!$('.modale').hasClass('opened'))
    document.querySelector("body").style.overflow = 'visible'; ///Ye background scroll wapis enable krne ke lie hai.
})

$("#upload-button-index").click(function(){
  $('#LogIn_Modal').modal();
});